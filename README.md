# Hello, I'm Shain! 👋 
[![LinkedIn Badge](https://img.shields.io/badge/LinkedIn-Profile-0077B5?style=flat&logo=linkedin&logoColor=white&color=0D76A8)](https://www.linkedin.com/in/shain-sahagun/) 
[![Microsoft Student Ambassadors Badge](https://img.shields.io/badge/Microsoft%20Learn%20Student%20Ambassadors-Profile-0078D7?style=flat&logo=microsoft&logoColor=white&color=0078D7)](https://mvp.microsoft.com/en-US/studentambassadors/profile/29029057-9590-40b8-8798-a96fdadaa7d8)
[![GitHub Profile](https://img.shields.io/badge/GitHub-Profile-orange?style=flat&logo=github&logoColor=white&color=562287)](https://github.com/m3mentomor1)
[![Portfolio Website](https://img.shields.io/badge/Portfolio-Visit%20My%20Website-4CAF50?style=flat&logo=web&logoColor=white&color=000000)](https://your-portfolio-website-url.com)

This is where you'll find all the projects I've worked on & am currently working on.

## 🙋 Introduction
I'm an undergraduate student currently pursuing a Bachelor of Science (BS) degree in **Computer Engineering (CpE)** at the Technological Institute of the Philippines (T.I.P.), Quezon City.

🚀 I'm also an aspiring multidisciplinary **Software Engineer** with a strong passion for gaining & developing skills in:
- **UI/UX Design, Front-end & Back-end Development** with specialization in
  - Mobile
  - Web
- **AI/ML Development** specializing in 
  - Computer Vision
  - Speech Processing
  - Natural Language Processing (NLP)
- **Cloud Development** specializing in integrating Microsoft Azure services into mobile & web apps.

*I love all things **dev**, & I thoroughly enjoy the process of creating & building things.*
##
## 🗂️ Projects

- [Mobile & Web: UI/UX Design, Front-end & Back-end Development]()

- [AI/ML](https://gitlab.com/m3mentomor1/m3mentomor1/-/blob/main/AI%5CML.md)

- [Microsoft Azure]()

- [Other Projects]() 

- [Solved LeetCode Problems]()
##
## 💻 Personal Tech Stack (Current)
| Area                        | Technologies                                                                                     |
|-----------------------------|--------------------------------------------------------------------------------------------------|
| **UI/UX Design**   | Mobile & Web: ``Figma`` ``Material Design 3`` ``Adobe Photoshop`` ``Canva``                      |
| **Front-end & Back-end Development** | Mobile: ``Dart`` ``Flutter`` ``Microsoft Azure`` ``PostgreSQL`` ``Git`` <br> Web: ``Python`` ``JavaScript`` ``CSS`` ``React`` ``Tailwind CSS`` ``Material UI`` ``Django`` ``PostgreSQL`` ``Microsoft Azure`` ``Git`` |
| **AI/ML**                         | ``Python`` ``TensorFlow`` ``OpenCV`` ``Keras`` ``NLTK`` ``spaCy`` ``Streamlit`` ``Microsoft Azure`` |
##
## ⚙️ Techologies/Tools I can use
|              |                                                                        |
|-----------------------------|--------------------------------------------------------------------------------------------------|
| **Programming/Markup/Style Sheet Language** | ``Python`` ``Java`` ``C++`` ``Kotlin`` ``Dart`` ``JavaScript`` ``HTML`` ``CSS`` |
| **Framework** | ``Flutter`` ``React`` ``Tailwind CSS`` ``Django`` ``Material UI`` ``PyQt`` ``Swing`` ``TensorFlow`` ``Keras`` ``NLTK`` ``spaCy`` |
| **Database** | ``SQLite`` ``PostgreSQL`` ``SQL`` ``MySQL`` |
| **Tools** | ``Figma`` ``Material Design 3`` ``Adobe Photoshop`` ``Canva`` ``Git`` ``GitHub`` ``GitLab`` ``Android Studio`` ``Postman`` ``OpenCV`` ``Streamlit``
| **Cloud Computing Platform** | ``Microsoft Azure`` |
| **Linux Ditros** | GNOME-based: ``Fedora`` ``Pop!_OS`` |
